$(".navbar").hide(); // Hide the navigation bar first

window.onscroll = () =>  {  // Listen for the window's scroll event   
    let scrollY = window.pageYOffset;  
    
    if(scrollY == document.body.scrollTop) {
        $('.navbar').fadeOut();
    } else {
        $('.navbar').fadeIn();        
    }       
}  