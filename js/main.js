$(document).ready(function () {

    // PRELOADER
    $('#preloader').delay(500).fadeOut('slow'); 

        // SCROLL ANIMATION
        smoothScroll.init({
            speed: 1000,
            easing: 'easeInOutCubic',
            updateURL: false,
            offset: 0,
            callbackBefore: function(toggle, anchor) {},
            callbackAfter: function(toggle, anchor) {}
        });

}); // document ready end


"use strict";
$(window).load(function () {
}); 
